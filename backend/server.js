const express = require('express');
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const config = require('./config');
const cors = require('cors');
const errorHandler = require('./handlers/error')

const {Product} = require('./models/productmodel');
const {Manufacture} = require('./models/manufacturemodel')

dotenv.config();
const app = express();
const PORT = process.env.PORT || 5000;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

    const mongodbUrl = config.MONGODB_URL;
      mongoose.connect(mongodbUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }).catch(error => console.log(error.reason));






app.get("/api/urunler/:id", async (req, res) => {
    const product = await Product.findOne({ _id: req.params.id })
    if(product) {
        res.send(product);
    } else {
        res.status(404).send({msg: "Ürün Bulunamadı."})
    }
});

app.get("/api/uretim/:id", async (req, res) => {
   // const manufactureId = req.params.id;
   // const manufacture = data1.manufactures.find(x => x._id === manufactureId);
   const manufacture = await Manufacture.findOne({ _id: req.params.id }) 
    if(manufacture) {
        res.send(manufacture);
    } else {
        res.status(404).send({msg: "Ürün Bulunamadı."})
    }
});

/*app.get("/api/urunler", (req, res) => {
    res.send(data.products);
})*/



app.get("/api/uretim", async (req, res) => {
    try{
       const manufactures = await Manufacture.find({});
        res.send(manufactures);
    } catch (e) {
        res.status(400).json({msg: e.message})
    }
})

app.get("/api/urunler", async (req, res) => {
    try{
        const products = await Product.find({});
        res.send(products);
    } catch (e) {
        res.status(400).json({msg: e.message})
    }

});

app.use(function(req, res, next) {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});

app.use(errorHandler);

app.listen(PORT, () => console.log(
    `Example app listening at http://localhost:${PORT}`,
  ));
